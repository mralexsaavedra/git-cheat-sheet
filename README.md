# Git Cheat Sheet

<hr>

<p align="center">
  <img src="https://github.com/arslanbilal/git-cheat-sheet/raw/master/Img/git-logo.png" height="190" width="455" title="Git Logo">
</p>

<hr>

### Index
* [Terminal config](#terminal-config)
* [Git global setup](#git-global-setup)
* [Create a new local repository](#create-a-new-local-repository)
* [Workflow](#workflow)
* [Add files](#add-files)
* [Commit](#commit)
* [Push](#push)
* [Status](#status)
* [Connect to a remote repository](#connect-to-a-remote-repository)
* [Branches](#branches)
* [Update from the remote repository](#update-from-the-remote-repository)
* [Tags](#tags)
* [Commit History](#commit-history)
* [Move / Rename](#move--rename)
* [Undo local changes](#undo-local-changes)
* [Rebase](#rebase)
* [Search](#search)
* [Create a new repository](#create-a-new-repository)
* [Existing folder](#existing-folder)
* [Existing Git repository](#existing-git-repository)

<hr>

### Terminal config
Configure your terminal in one of these two files: .bashrc (Git Bash) or .bash_profile
```
PS1='\[\033[1;36m\]\W\[\033[35m\]@\[\033[0m\]\[\033[1;34m\]\u\[\033[1;32m\]$(__git_ps1) \[\033[0m\]\$ '

# \W            : directory
# \u            : user
# $(__git_ps1)  : branch
```

### Git global setup
```
git config --global user.name "example"             # Configure the author name to be used with your commits

git config --global user.email "example@gmail.com"  # Configure the author email address to be used with your commits

git config --global color.ui true                   # Add colors              

git config --global core.editor "notepad++"         # Edit default editor
```

### Create a new local repository
```
git init
```

### Check out a repository                    
```
git clone /path/to/repository                   # Create a working copy of a local repository      

git clone username@host:/path/to/repository     # For a remote server
```

### Workflow
![workflow](http://rogerdudler.github.io/git-guide/img/trees.png)

### Add files
```
# Add one or more files to staging 

git add <filename>

git add <folder>

git add *.md

git add .
```

### Commit
```
git commit                                    # Large message

git commit -m "Commit message"                # Commit changes to head (but not yet to the remote repository)

git commit -a                                 # Commit any files you've added with git add, and also commit any files you've changed since then

git commit -v                                 # Diff in the editor

git commit -a -m "Commit message"             # Automatically stage tracked files

git commit -am 'message here'                 # Commit skipping the staging area and adding message

git commit --date="`date --date='n day ago'`" -am "<Commit Message Here>" # Commit to some previous date:

git commit --amend                            # Change the last commit

# Move uncommitted changes from current branch to some other branch:
git stash
git checkout branch2
git stash pop
```

### Push
```
git push origin master                        # Send changes to the master branch of your remote repository
```

### Status
```
git status                                    # List the files you've changed and those you still need to add or commit
```

### Connect to a remote repository
```
git remote add origin <server>                # If you haven't connected your local repository to a remote server, add the server to be able to push to it

git remote -v                                 # List all currently configured remote repositories

git remote show <remote>                      # Show information about a remote

git fetch <remote>                            # Download all changes from <remote>, but don’t integrate into HEAD
```

### Branches
```
git checkout -b <branchname>                  # Create a new branch and switch to it

git checkout <branchname>                     # Switch from one branch to another

git branch                                    # List all the branches in your repo, and also tell you what branch you're currently in

git branch -a                                 # Show all branchs

git branch -r                                 # Show remote branchs

git branch -d <branchname>                    # Delete the feature branch

git branch -D <branchname>                    # Force delete the feature branch

git push origin <branchname>                  # Push the branch to your remote repository, so others can use it

git push --all origin                         # Push all branches to your remote repository

git push origin :<branchname>                 # Delete a branch on your remote repository
```

### Update from the remote repository
```
git pull                                      # Fetch and merge changes on the remote server to your working directory

git merge <branchname>                        # To merge a different branch into your active branch

# View all the merge conflicts

git diff                                      # working copy > staging area

git diff --staged                             # working copy > repository

git diff HEAD                                 # working copy > repository

git add <filename>                            # After you have manually resolved any conflicts, you mark the changed file
```

### Tags
```
git tag 1.0.0 <commitID>                      # You can use tagging to mark a significant changeset, such as a release

git tag -d <tag_name>                         # Delete

git push --tags origin                        # Push all tags to remote repository
```

### Commit History
```
git log                                       # CommitId is the leading characters of the changeset ID, up to 10, but must be unique. Get the ID using

git log --oneline                             # Show all the commits(it'll show just the commit hash and the commit message)

git log --author="username"                   # Show all commits of a specific user

git log -p <file>                             # Show changes over time for a specific file

git blame <file>                              # Who changed what and when in <file>

git reflog show                               # Show Reference log

git reflog delete                             # Delete Reference log
```

### Move / Rename
```
git mv Index.txt Index.html                   # Rename Index.txt to Index.html
```

### Undo local changes
```
# If you mess up, you can replace the changes in your working tree with the last content in head:
# Changes already added to the index, as well as new files, will be kept

git checkout -- <filename>                    # Discard local changes in a specific file

git reset HEAD~1                              # Undo the last commit but not remove working copy

git reset --hard HEAD~1                       # Undo the last commit and remove working copy

git revert <commit>                           # Revert a commit (by producing a new commit with contrary changes)

git reset --hard <commit>                     # Reset your HEAD pointer to a previous commit and discard all changes since then

```

### Rebase
```
git rebase <branch>                           # Rebase your current HEAD onto <branch>

git rebase --abort                            # Abort a rebase

git rebase --continue                         # Continue a rebase after resolving conflicts
```

### Search
```
git grep "foo()"                              # Search the working directory for foo()
```

### Create a new repository
```
git clone URL
cd project
git add .
git commit -m "first commit"
git push -u origin master
```

### Existing folder
```
cd project
git init
git remote add origin URL
git add .
git commit -m "first commit"
git push -u origin master
```

### Existing Git repository
```
cd project
git remote rename origin old-origin
git remote add origin URL
git push -u origin --all
git push -u origin --tags
```
